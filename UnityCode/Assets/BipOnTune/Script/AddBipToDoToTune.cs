﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBipToDoToTune : MonoBehaviour
{
    
    public BipOnTuneReplay m_replay;
    public BipToDo m_toDoToAdd;


    public void OnValidate()
    {
        if (m_toDoToAdd != null)
        {
            m_toDoToAdd.m_keyFrame.m_time = m_replay.GetTime();
            m_toDoToAdd = null;
        }
    }
}
