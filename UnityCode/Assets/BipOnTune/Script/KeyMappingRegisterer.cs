﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyMappingRegisterer : MonoBehaviour {

    public BipOnTune m_bipOnTune;
    public KeyCode m_registerKey;
    public bool m_useDown;
    public TuneKeyFrame m_framePatternDown;
    public bool m_useUp;
    public TuneKeyFrame m_framePatternUp;
    public GetTimeFrom m_time;
	

	void Update () {
        if (m_useDown && Input.GetKeyDown(m_registerKey))
        {
                m_bipOnTune.AddKeyFrame(GetTime(), m_framePatternDown);
        }
        if (m_useUp && Input.GetKeyUp(m_registerKey))
        {
            m_bipOnTune.AddKeyFrame(GetTime(), m_framePatternUp);
        }
    }

    public virtual float GetTime()
    {
        return m_time.GetTime();
    }
}
