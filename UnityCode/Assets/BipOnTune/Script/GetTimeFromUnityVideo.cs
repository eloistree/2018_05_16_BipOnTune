﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class GetTimeFromUnityVideo : GetTimeFrom
{
    public VideoPlayer m_videoPlayer;
    public override float GetTime()
    {
        return (float) m_videoPlayer.time;
    }
}
