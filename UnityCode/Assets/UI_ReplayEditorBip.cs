﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_ReplayEditorBip : MonoBehaviour {

    public InputField m_description;
    public InputField m_timer;
    public TuneKeyFrame m_frame;
    public Button m_addTime;
    public Button m_removeTime;
    public Button m_removeFrame;
    

    [System.Serializable]
    public class FrameChanged : UnityEvent<UI_ReplayEditorBip> { }
    



    public void SetInfo(string description, float timer) {
        m_description.text = description;
        m_timer.text = timer.ToString();
    }
    public string GetDescription() { return m_description.text; }
    public float GetTimer() {
      return  float.Parse(m_timer.text);
    }
    
    

    internal void SetPourcent(float pct)
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.anchorMin = new Vector2(0,pct);
        rect.anchorMax = new Vector2(1,pct);
    }

    internal void SetFrame(TuneKeyFrame tuneKeyFrame)
    {
        m_frame = tuneKeyFrame;

    }
    public TuneKeyFrame GetOriginalFrame()
    {
        return m_frame;
    }
    public TuneKeyFrame GetCurrentFrame()
    {
        return new TuneKeyFrame() {
            m_descriptionName = m_description.text,
            m_time = float.Parse(m_timer.text) };
    }
}

